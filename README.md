# Ask me anything!

Hello there! 👋

Thanks a lot for getting in touch with me! This is my [AMA](https://en.wikipedia.org/wiki/Reddit#IAmA_and_AMA).

I get a lot of questions by email. This way anyone can read the answer!
Anything means _anything_. Personal questions. Money. Work. Life. Code. Whatever.

| [Ask a question ](https://gitlab.com/yo/ama/-/issues/new) 🤔 | [Read questions ](https://gitlab.com/yo/taskord/-/issues) 🤓 |
| :----- | :----- |


## Other ways to connect:

### Do you do Emails?

Absolutely, but please be patient. I do get a ton of emails, notifications & alerts. With the amount of spam these days, its easy to miss an important email such as yours! 🤷🏽‍♂️

**GitLab is the fastest way to reachout**. [Consider a public ama](https://gitlab.com/yo/ama/-/issues/new), if your query does not warrant email. 

Note: Pitching me a service, product or similar for my employer will be ignored straight up, so don't bother.

Email me here: `yoginth+ama [at] icloud.com`, I will try and post some of the queries here redacting as much as I can for benefit of others.

### How about Twitter?

Yes, why not? My DMs are open. Give me a follow 😉. I tweet (and retweet fun stuff).

> <https://twitter.com/big1nt>

---

## Guidelines

- Ensure your question hasn't already been answered.
- Use a [succinct](https://dictionary.cambridge.org/dictionary/english/succinct) title and description.
- Bugs & feature requests should be opened on the relevant issue tracker.
- Support questions are better asked on Stack Overflow.
- Be civil and polite. Follow the [code of conduct](./CODE_OF_CONDUCT.md).

## Links

- [Read more AMAs](https://github.com/sindresorhus/amas)
- [Create your own AMA](https://github.com/sindresorhus/amas/blob/master/create-ama.md)
- [What's an AMA?](https://en.wikipedia.org/wiki/Reddit#IAmA_and_AMA)
